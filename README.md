# Example Portlet Using Grunt.js

[Grunt.js ](http://gruntjs.com/) is a task runner tool for JavaScript,
similar to Ant for Java.  It has a bunch of plugins and is super useful
for anything web-related, but especially JavaScript-y stuff.

This project is an example [Liferay](http://liferay.com) Portlet that
incorporates Grunt into the usual Ant build and deploy task.  Specifically,
this example demonstrates linting JavaScript code to prevent common
JavaScript mistakes.

